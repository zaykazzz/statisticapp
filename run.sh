hadoop fs -rm -r /user/cloudera/*RES/
echo "Removing output directory..."
spark-submit --class com.test.statisticApp.StatisticApp --master local target/statisticApp-1.0-SNAPSHOT-jar-with-dependencies.jar
echo "Hadoop work complete"
echo "Removing output file in local directory"
rm -f ./outputFile
hadoop fs -text /user/cloudera/RES/part* > ./outputfile
#hadoop fs -text /user/cloudera/SalRES/part* > ./outputfileSalary
echo "Results saved in ./outputFile"
