package com.test.statisticApp;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

/**
 * Hello world!
 *
 */
public class StatisticApp
{
    public static void main( String[] args )
    {

        SparkConf conf =  new SparkConf()
                .setAppName("StatisticApp");

        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> textFileData = sc.textFile("hdfs:///user/cloudera/person/part-m-00000");

        JavaPairRDD<Integer, Tuple2<Iterable<Double>, Iterable<Integer>>> result = calculateString(textFileData);

        result.saveAsTextFile("hdfs:///user/cloudera/RES/");

        sc.stop();
    }

    public static JavaPairRDD<Integer, Tuple2<Iterable<Double>, Iterable<Integer>>> calculateString(JavaRDD<String> textFileData)
    {
        // for Salary
        final JavaPairRDD<Integer, Double> salaryText = textFileData.mapToPair(
                new PairFunction<String, Integer, Double>() {
                    @Override
                    public Tuple2<Integer, Double> call(String s) {
                        Person p = new Person(s);
                        return new Tuple2<Integer, Double>(Integer.valueOf(p.ageCategory), Double.valueOf(p.salary));
                    }
                }
        );

        JavaPairRDD<Integer, Double> reducedSalaryText = salaryText.reduceByKey(
                new Function2 <Double, Double, Double>() {
                    public Double call(final Double value0, final Double value1) {
                        return value0 + value1;
                    }
                });

        // for Trips
        final JavaPairRDD<Integer, Integer> tripsText = textFileData.mapToPair(
                new PairFunction<String, Integer, Integer>() {
                    @Override
                    public Tuple2<Integer, Integer> call(String s) {
                        Person p = new Person(s);
                        return new Tuple2<Integer, Integer>(Integer.valueOf(p.ageCategory), Integer.valueOf(p.trips));
                    }
                }
        );

        JavaPairRDD<Integer, Integer> reducedTripsText = tripsText.reduceByKey(
                new Function2 <Integer, Integer, Integer>() {
                    public Integer call(final Integer value0, final Integer value1) {
                        return value0 + value1;
                    }
                });

        JavaPairRDD<Integer, Tuple2<Iterable<Double>, Iterable<Integer>>> result = reducedSalaryText.groupWith(reducedTripsText);

        return result;

    }

}


