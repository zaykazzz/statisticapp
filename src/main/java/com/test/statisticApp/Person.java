package com.test.statisticApp;

import static java.lang.Math.min;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.text.MessageFormat;
import java.util.ArrayList;

/**
 *  Age category:
 *  0..10 - 1
 *  11..20 - 2
 *  21..30 - 3
 *  31..40 - 4
 *  41..50 - 5
 *  51..60 - 6
 *  61..70 - 7
 *  71..80 - 8
 *  81..90 - 9
 *  > 91 - 10
 */



public class Person implements Serializable {
    public static int countOfMounths = 0;
    public static ArrayList months = new ArrayList();

    public int ageCategory;
    public double salary;
    public int trips;
    public int month;

    private int age;

    public Person(Person p)
    {
        this.age = p.age;
        this.month = p.month;
        if (!this.months.contains(this.month))
        {
            this.months.add(this.month);
            countOfMounths++;
        }
        this.salary = p.salary;
        this.trips = p.trips;
        this.ageCategory = p.ageCategory;
    }

    public Person(String text)
    {
        String params[] = text.split(",");

        if (params.length < 3) {
            this.ageCategory = -1;
            return;
        }

        this.age = Integer.parseInt( params[0] );

        this.month = Integer.parseInt( params[1] );

        if (!this.months.contains(this.month))
        {
            this.months.add(this.month);
            countOfMounths++;
        }

        if (params[2].contains(".0"))
        {

            this.trips = (int) Double.parseDouble( params[2] );
            this.salary = 0;
        } else {
            this.salary = Double.parseDouble(params[2]);
            this.trips = 0;
        }

        this.ageCategory = getAgeCategoryfromAge();
    }

    private int getAgeCategoryfromAge()
    {
        int decades = (this.age / 10) + 1;

        return min(decades, 10);
    }

    /**
     Compare two Persons
     */
    public int compareTo(Person person)
    {
        if(this.ageCategory < person.ageCategory)
            return -1;

        if(this.ageCategory > person.ageCategory)
            return 1;

        return 0;
    }

    /**
     equals of two Persons
     */
    public boolean equals(Object obj)
    {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Person)) {
            return false;
        }
        Person person = (Person) obj;
        if (this.ageCategory != person.ageCategory)
            return false;
        if (this.month != person.month)
            return false;

        if (this.age != person.age)
            return false;

        if (this.salary != person.salary)
            return false;

        if (this.trips != person.trips)
            return false;



        return true;
    }

    /**
     Hash code
     */
    public int hashCode()
    {
        return this.ageCategory;
    }

    /**
     To string
     */
    @Override
    public String toString()
    {
        return MessageFormat.format( "{0},{1},{2}", ageCategory, salary, trips );
    }
}
