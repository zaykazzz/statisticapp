package com.test.statisticApp;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class StatisticAppTest
    extends TestCase
{

    /**
     * Check right creation of Person object
     */
    public  void testPerson()
    {
        Person p = new Person("36,12,5.0");

        assertEquals("Wrong month",p.month,12);
        assertEquals("Wrong ageCategory",p.ageCategory,4);
        assertEquals("Wrong trips",p.trips,5);
        assertEquals(p.salary,0.0);
    }

    /**
     * Check age category when few params
     */
    public void testPersonFewArgs()
    {
        Person p = new Person("36,12");
        assertEquals("Wrong ageCategory",p.ageCategory, -1);
    }

    /**
     * Check if equals objects return true
     */
    public void testPersonEqualObjects()
    {
        Person p = new Person("36,12,5.0");
        Person p2 = new Person("36,12,5.0");

        boolean res = p.equals(p2);
        assertEquals("Objects equal error!",res, true);
    }

    /**
     * Check if non equals objects return false
     */
    public void testPersonNonEqualObjects()
    {
        Person p = new Person("36,12,5.0");
        Person p2 = new Person("35,14,5.0");

        boolean res = p.equals(p2);
        assertEquals("Objects non equal error!",res, false);
    }

    /**
     * Check hash method
     */
    public void testHash()
    {
        Person p = new Person("36,12,5.0");

        int hash = p.hashCode();

        assertEquals("Wrong hash",hash,4);
    }

//    public void testCalculate()
//    {
//        SparkConf conf =  new SparkConf()
//                .setAppName("StatisticApp");

//        JavaSparkContext sc = new JavaSparkContext();
//
//        List<String> dataStrings = Arrays.asList
//                (
//                        "2,5,8.0",
//                        "2,7,6.0",
//                        "2,5,22.25",
//                        "2,4,8.54"
//                );
//
//        JavaSparkContext sc = new JavaSparkContext();
//        JavaRDD<String> textData =  sc.parallelize( dataStrings );
//
//        HashMap<Integer, Tuple2<Double, Integer>> expResults = new HashMap<>();
//        expResults.put( new Integer(2), new Tuple2<Double, Integer>(Double.valueOf(30.79), Integer.valueOf(14) ));
//
//        JavaPairRDD<Integer, Tuple2<Iterable<Double>, Iterable<Integer>>> realResults = StatisticApp.calculateString(textData);
//
//        String s = realResults.toString();
//
//        assertEquals("JavaRDD string error",s, "");


//    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public StatisticAppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( StatisticAppTest.class );
    }




}
